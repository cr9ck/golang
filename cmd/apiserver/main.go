package main

import (
	"bitbucket.org/cr9ck/golang/internal/app/apiserver"
	"bitbucket.org/cr9ck/golang/internal/app/apiserver/loggers"
	"flag"
	"github.com/BurntSushi/toml"
	"log"
)

var (
	configPath string
)

func init() {
	flag.StringVar(
		&configPath,
		"config-path",
		"configs/apiserver.toml",
		"path to config file")
}

func main() {
	flag.Parse()

	config:= apiserver.GetInstance()
	loggers.Instance(config.LogLevel)

	_, err := toml.DecodeFile(configPath, config)

	if err != nil {
		log.Fatal(err)
	}
	s:= apiserver.Instance(config)
	if err:= s.Start(); err != nil {
		log.Fatal(err)
	}
}
