module bitbucket.org/cr9ck/golang

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/gorilla/mux v1.7.4
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.2.2
)
