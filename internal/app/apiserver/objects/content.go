package objects

type ContentMarketing struct {
	Type_              string  `json:"type"`
	HarvesterId       string  `json:"harvesterId"`
	CommercialPartner string  `json:"commercialPartner"`
	LogoURL           string  `json:"logoURL"`
	CerebroScore      float32 `json:"cerebro-score"`
	Url               string  `json:"url"`
	Title             string  `json:"title"`
	CleanImage        string  `json:"cleanImage"`
}

type DefaultContentMarketing struct {
	Type_              string  `json:"type"`
}

func GetDefaultContentMarketing() *DefaultContentMarketing {
	return &DefaultContentMarketing{Type_: "Ad"}
}

type ContentMarketingContainer struct {
	Status   int `json:"httpStatus"`
	Response struct {
		ContentMarketings []ContentMarketing `json:"items"`
	} `json:"response"`
}

func (contentMarketingContainer *ContentMarketingContainer) GetContentMarketings() []ContentMarketing {
	return contentMarketingContainer.Response.ContentMarketings
}

func (contentMarketing *ContentMarketing) Type() string {
	return contentMarketing.Type_
}

func (defaultContentMarketing *DefaultContentMarketing) Type() string {
	return defaultContentMarketing.Type_
}
