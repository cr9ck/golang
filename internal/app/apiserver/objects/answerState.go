package objects

type Type interface {
	Type() string
}

type Answer struct {
	Data *[] Type
}
