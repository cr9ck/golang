package objects

type Article struct {
	Type_        string  `json:"type"`
	HarvesterId  string  `json:"harvesterId"`
	CerebroScore float32 `json:"cerebro-score"`
	Url          string  `json:"url"`
	Title        string  `json:"title"`
	CleanImage   string  `json:"cleanImage"`
}

func (article *Article) Type() string {
	return article.Type_
}

type ArticleContainer struct {
	Status int `json:"httpStatus"`
	Response struct{
		Articles []Article `json:"items"`
	} `json:"response"`
}

func (articleContainer *ArticleContainer) GetArticles() []Article {
	return articleContainer.Response.Articles
}