package handlers

import (
	"bitbucket.org/cr9ck/golang/internal/app/apiserver/loggers"
	"bitbucket.org/cr9ck/golang/internal/app/apiserver/objects"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

var (
	articlesEndpoint         = "https://storage.googleapis.com/aller-structure-task/articles.json"
	contentMarketingEndpoint = "https://storage.googleapis.com/aller-structure-task/contentmarketing.json"
)

type Handler struct{}

func Instance() *Handler {
	return &Handler{}
}

func (handler *Handler) GetRequestHandler() http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		answer := handler.getFormedAnswer()
		writer.Header().Set("Content-type", "application/json")
		_, _ = io.WriteString(writer, answer)
	}
}

func (handler *Handler) getFormedAnswer()  string {

	articles := handler.getArticles()
	contentMarketingSlice := handler.getContentMarketings()
	var formedAnswer = handler.getFormedArray(articles, contentMarketingSlice)

	return formatArrayToString(formedAnswer)
}

func (handler *Handler) getFormedArray(articles []objects.Article, marketingSlice []objects.ContentMarketing) [] objects.Type{

	var formedAnswer [] objects.Type

	for articleIndex, article := range articles {

		formedAnswer = append(formedAnswer, &article)
		if (articleIndex + 1)%5 == 0 {
			marketingIndex := articleIndex/5
			var contentMarketing objects.Type = objects.GetDefaultContentMarketing()
			if marketingIndex < len(marketingSlice) {
				contentMarketing = &marketingSlice[marketingIndex]
			}
			formedAnswer = append(formedAnswer, contentMarketing)
		}
	}
	return formedAnswer
}

func formatArrayToString(data [] objects.Type) string {

	var answer []string

	for _, object := range data {
		marshal, _ := json.Marshal(&object)
		answer = append(answer, string(marshal))
	}
	return "[" + strings.Join(answer, ",\n") + "]"
}

func (handler *Handler) getArticles() []objects.Article {
	var articles objects.ArticleContainer
	handler.makeRequest(articlesEndpoint, &articles)
	loggers.Instance("debug").Info("articles: ", len(articles.GetArticles()))
	return articles.GetArticles()
}

func (handler *Handler) getContentMarketings() []objects.ContentMarketing {
	var contentMarketing objects.ContentMarketingContainer
	handler.makeRequest(contentMarketingEndpoint, &contentMarketing)
	loggers.Instance("debug").Info("contentMark: ", len(contentMarketing.GetContentMarketings()))
	return contentMarketing.GetContentMarketings()
}

func (handler *Handler) makeRequest(endpoint string, dataHolder interface{}) {
	response, err := http.Get(endpoint)
	if err != nil {
		loggers.Instance("debug").Info("Error while making request to ", endpoint)
		log.Fatalln(err)
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		loggers.Instance("debug").Info("Error while reading response body")
		log.Fatalln(err)
	}

	_ = json.Unmarshal(body, dataHolder)
}
