package loggers

import (
	"github.com/sirupsen/logrus"
	"sync"
)

var instance *Logger = nil
var once sync.Once

type Singleton interface {
	Info(args ...interface{})
}

type Logger struct {
	logger *logrus.Logger
}

func (logger *Logger) Info(args ...interface{}) {
	logger.logger.Info(args)
}

func Instance(logLvl string) Singleton {
	once.Do(func() {
		instance = &Logger{logger: logrus.New()}

		level, err := logrus.ParseLevel(logLvl)
		if err == nil {
			instance.logger.SetLevel(level)
		}
	})
	return instance
}
