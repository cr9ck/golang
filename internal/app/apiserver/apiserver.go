package apiserver

import (
	"bitbucket.org/cr9ck/golang/internal/app/apiserver/loggers"
	"bitbucket.org/cr9ck/golang/internal/app/apiserver/routers"
	"net/http"
)

type ApiServer struct {
	config     *Config
	controller *routers.Router
}

func Instance(config *Config) *ApiServer {
	return &ApiServer{
		config:     config,
		controller: routers.Instance(),
	}
}

func (apiServer *ApiServer) Start() error {
	loggers.Instance("debug").Info("starting api server")
	return http.ListenAndServe(apiServer.config.BindAddr, apiServer.controller.GetRouter())
}
