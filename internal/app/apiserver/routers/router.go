package routers

import (
	"bitbucket.org/cr9ck/golang/internal/app/apiserver/handlers"
	"github.com/gorilla/mux"
)

var (
	getArticleWithContentEndpoint = "/getArticleWithContent"
)

type Router struct {
	router *mux.Router
}

func Instance() *Router {
	handler := handlers.Instance()
	router := Router{router: mux.NewRouter()}
	router.setRequestHandlers(handler)

	return &router
}

func (router *Router) setRequestHandlers(handler *handlers.Handler) {
	router.router.HandleFunc(getArticleWithContentEndpoint, handler.GetRequestHandler())
}

func (router *Router) GetRouter() *mux.Router {
	return router.router
}
